FROM tomcat:8.5.56

COPY . /project

RUN cd /project

WORKDIR /project

EXPOSE 8080

CMD ["catalina.sh", "run"]